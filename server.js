const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

// Middleware to parse JSON data
app.use(bodyParser.json());

// Sample data (Replace this with your data source)
let data = [
  { id: 1, name: 'John' },
  { id: 2, name: 'Jane' },
];

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// GET endpoint to fetch data
app.get('/api/data', (req, res) => {
  res.json(data);
});

// POST endpoint to submit data
app.post('/api/data', (req, res) => {
  const newData = req.body;
  data.push(newData);
  res.status(201).json(newData);
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
